package com.example.matrimoney.Model;

import java.io.Serializable;

public class LanguageModel implements Serializable {
    int LanguageID;
    String Name;



    public int getLanguageID() {
        return LanguageID;
    }

    public void setLanguageID(int languageID) {
        LanguageID = languageID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
