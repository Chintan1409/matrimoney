package com.example.matrimoney.activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.matrimoney.Model.CityModel;
import com.example.matrimoney.Model.LanguageModel;
import com.example.matrimoney.Model.UserModel;
import com.example.matrimoney.R;
import com.example.matrimoney.adapter.CityAdapter;
import com.example.matrimoney.adapter.LanguageAdapter;
import com.example.matrimoney.database.TblMstCity;
import com.example.matrimoney.database.TblMstLanguage;
import com.example.matrimoney.database.TblUser;
import com.example.matrimoney.fragment.UserListFragment;
import com.example.matrimoney.util.Constant;
import com.example.matrimoney.util.Utils;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener{
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etFatherName)
    TextInputEditText etFatherName;
    @BindView(R.id.etSurName)
    TextInputEditText etSurName;
    @BindView(R.id.rbMale)
    MaterialRadioButton rbMale;
    @BindView(R.id.rbFeMale)
    MaterialRadioButton rbFeMale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etDob)
    TextInputEditText etDob;
    @BindView(R.id.etPhoneNumber)
    TextInputEditText etPhoneNumber;
    @BindView(R.id.etEmailAddress)
    TextInputEditText etEmailAddress;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.screen_Layout)
    FrameLayout screenLayout;

    String startingDate = "2000-09-14";

    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> cityList = new ArrayList<>();
    ArrayList<LanguageModel> languageList = new ArrayList<>();

    UserModel userModel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user_activity);
        setUpActionBar(getString(R.string.lbl_deshbord), true);
        ButterKnife.bind(this);
        setDataToView();
        getDataForUpdate();
        setTypefaceOnView();

    }
    void setTypefaceOnView() {
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/NotoSerif-Bold.ttf");
        Typeface type1 = Typeface.createFromAsset(getAssets(),"fonts/NotoSerif-BoldItalic.ttf");
        Typeface type2 = Typeface.createFromAsset(getAssets(),"fonts/NotoSerif-Italic.ttf");
        etName.setTypeface(type1);
        etFatherName.setTypeface(type1);
        etPhoneNumber.setTypeface(type1);
        btnSubmit.setTypeface(type1);
        etSurName.setTypeface(type1);
        rbMale.setTypeface(type1);
        rbFeMale.setTypeface(type1);
        etDob.setTypeface(type1);
        etEmailAddress.setTypeface(type1);
        btnSubmit.setTypeface(type1);

    }

    void getDataForUpdate(){
        if (getIntent().hasExtra(Constant.USER_OBJECT)){
        userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
        getSupportActionBar().setTitle(R.string.lbl_edit_user);
        etName.setText(userModel.getName());
        etFatherName.setText(userModel.getFatherName());
        etSurName.setText(userModel.getSurName());
        etEmailAddress.setText(userModel.getEmail());
        etPhoneNumber.setText(userModel.getPhoneNumber());
        etDob.setText(userModel.getDob());
        if (userModel.getGender() == Constant.MALE){
            rbMale.setChecked(true);
        }
        else{
            rbFeMale.setChecked(true );
        }
        spCity.setSelection(getSelectedPositionFromCityID(userModel.getCityID()));
        spLanguage.setSelection(getSelectedPositionFromLanguageID(userModel.getLanguageID()));
    }}

    int getSelectedPositionFromCityID(int cityID){
        for(int i=0;i<cityList.size();i++){
            if (cityList.get(i).getCityID()==cityID){
                return  i ;
            }
        }
        return 0;
    }
    int getSelectedPositionFromLanguageID(int langugeID){
        for(int i=0;i<languageList.size();i++){
            if (cityList.get(i).getCityID()==langugeID){
                return  i ;
            }
        }
        return 0;
    }
    void setSpinnerAdapter() {
        cityList.addAll(new TblMstCity(this).getCityList());
        languageList.addAll(new TblMstLanguage(this).getLanguages());

        cityAdapter = new CityAdapter(this, cityList);
        languageAdapter = new LanguageAdapter(this, languageList);

        spCity.setAdapter(cityAdapter);
        spLanguage.setAdapter(languageAdapter);

    }

    void setDataToView() {
        final Calendar newCalendar = Calendar.getInstance();
        etDob.setText(
                String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" + String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" + String.format("%02d", newCalendar.get(Calendar.YEAR)));
        setSpinnerAdapter();
    }

    @OnClick(R.id.etDob)
    public void onEtDobClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(startingDate);
        newCalendar.setTimeInMillis(date.getTime());
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                AddUserActivity.this, (datePicker, year, month, day) -> etDob.setText(String.format("%02d", day) + "/" + String.format("%02d", (month + 1)) + "/" + year),
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }

    boolean isValidUser() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            isValid = false;
            etName.setError(getString(R.string.lbl_name));
        }

        if (TextUtils.isEmpty(etFatherName.getText().toString())) {
            isValid = false;
            etFatherName.setError(getString(R.string.error_father_name));
        }

        if (TextUtils.isEmpty(etSurName.getText().toString())) {
            isValid = false;
            etSurName.setError(getString(R.string.error_enter_surname));
        }

        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_enter_phone));
        } else if (etPhoneNumber.getText().toString().length() < 10) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_valid_phone));
        }

        if (TextUtils.isEmpty(etEmailAddress.getText().toString())) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_ente_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.getText().toString()).matches()) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_valid_email));
        }

        if (spCity.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_city_select));
        }
        if (spLanguage.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_language_select));
        }
        return isValid;
    }

    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {
        if (isValidUser()){
            if(userModel == null){
                long lastInsertedID = new TblUser(getApplicationContext()).insertUser(etName.getText().toString(), etFatherName.getText().toString()
                        ,etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE, Utils.getFormatedDateToInsert(etDob.getText().toString())
                        , etPhoneNumber.getText().toString(),etEmailAddress.getText().toString(),cityList.get(spCity.getSelectedItemPosition()).getCityID(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(),0);
                showToast(lastInsertedID > 0 ? "User Inserted Successfully" : "Something went wrong");
            }else{
                long lastInsertedID = new TblUser(getApplicationContext()).updateUserByID(userModel.getUserID(),etName.getText().toString(), etFatherName.getText().toString()
                        ,etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE, Utils.getFormatedDateToInsert(etDob.getText().toString())
                        , etPhoneNumber.getText().toString(),etEmailAddress.getText().toString(),cityList.get(spCity.getSelectedItemPosition()).getCityID(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(),userModel.getIsFavorite());
                showToast(lastInsertedID > 0 ? "User Updated Successfully" : "Something went wrong");

            }
            Intent intent = new Intent(AddUserActivity.this,ActivityUserListByGender.class);
            startActivity(intent);
            finish();

        }
    }
}

