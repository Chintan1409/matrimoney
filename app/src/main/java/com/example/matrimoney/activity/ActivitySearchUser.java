package com.example.matrimoney.activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.matrimoney.Model.UserModel;
import com.example.matrimoney.R;
import com.example.matrimoney.adapter.UserListAdapter;
import com.example.matrimoney.database.TblUser;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySearchUser extends BaseActivity {
    @BindView(R.id.etUserSearch)
    EditText etUserSearch;
    @BindView(R.id.rcvUsers)
    RecyclerView rcvUsers;
    ArrayList<UserModel> userList = new ArrayList<>();
    ArrayList<UserModel> tempuserList = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serch_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_serach_user), true);
        setAdapter();
        setSearchUser();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.ivFavoriteUser) {
            Intent intent = new Intent(this, FavoriteUserActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
    void resetAdapter() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }
    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(ActivitySearchUser.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure want to delete this user?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedUserId = new TblUser(ActivitySearchUser.this).deleteUserById(userList.get(position).getUserID());
                    if (deletedUserId > 0) {
                        Toast.makeText(ActivitySearchUser.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(ActivitySearchUser.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }
    void setSearchUser() {
        etUserSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tempuserList.clear();
                if (charSequence.toString().length() > 0) {
                    for (int j = 0; j < userList.size(); j++) {
                        if (userList.get(j).getName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getFatherName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getSurName().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getPhoneNumber().toLowerCase().contains(charSequence.toString().toLowerCase())
                                || userList.get(j).getEmail().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            tempuserList.add(userList.get(j));
                        }
                    }
                }
                if (charSequence.toString().length() == 0 && tempuserList.size() == 0) {
                    tempuserList.addAll(userList);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    void setAdapter() {
        rcvUsers.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getUserList());
        tempuserList.addAll(userList);
        adapter = new UserListAdapter(this, tempuserList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUser = new TblUser(ActivitySearchUser.this).updateFavoriteStatus(userList.get(position).getIsFavorite() == 0 ? 1 : 0, userList.get(position).getUserID());
                if (lastUpdatedUser > 0) {
                    userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0 ? 1 : 0);
                    adapter.notifyItemChanged(position);
                }
            }

            @Override
            public void onItemClick(int position) {

            }
        });
        rcvUsers.setAdapter(adapter);
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUsers.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUsers.setVisibility(View.GONE);
        }
    }

}
