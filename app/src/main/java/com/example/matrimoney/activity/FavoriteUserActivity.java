package com.example.matrimoney.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimoney.Model.UserModel;
import com.example.matrimoney.R;
import com.example.matrimoney.adapter.UserListAdapter;
import com.example.matrimoney.database.TblUser;
import com.example.matrimoney.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteUserActivity extends BaseActivity {
    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_userlist), true);
        setAdapter();
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(FavoriteUserActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure want to delete this user?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedUserId = new TblUser(FavoriteUserActivity.this).deleteUserById(userList.get(position).getUserID());
                    if (deletedUserId > 0) {
                        Toast.makeText(FavoriteUserActivity.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        CheckAndVisibleView();
                    } else {
                        Toast.makeText(FavoriteUserActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(this, 1));
        userList.addAll(new TblUser(this).getFavoriteUserList());
        adapter = new UserListAdapter(this, userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onFavoriteClick(int position) {
                int lastUpdatedUser = new TblUser(FavoriteUserActivity.this).updateFavoriteStatus(0, userList.get(position).getUserID());
                if (lastUpdatedUser > 0) {
                    sendFavoriteChangeBroadCast(userList.get(position));
                    userList.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(0, userList.size());

                }
            }

            @Override
            public void onItemClick(int position) {

            }
        });
        rcvUserList.setAdapter(adapter);
    }

    void sendFavoriteChangeBroadCast(UserModel userModel) {
        Intent intent = new Intent(Constant.FAVORITE_CHANGE_FILTER);
        intent.putExtra(Constant.USER_ID, userModel);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    void CheckAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

}
