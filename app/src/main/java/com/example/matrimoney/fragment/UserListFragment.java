package com.example.matrimoney.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimoney.Model.UserModel;
import com.example.matrimoney.R;
import com.example.matrimoney.activity.AddUserActivity;
import com.example.matrimoney.adapter.UserListAdapter;
import com.example.matrimoney.database.TblUser;
import com.example.matrimoney.util.Constant;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserListFragment extends Fragment {

    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;

    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;

    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;
    @BindView(R.id.btnAddUser)
    FloatingActionButton btnAddUser;

    MyFavoriteChange myFavoriteChange = new MyFavoriteChange();

    public static UserListFragment getInstance(int gender) {
        UserListFragment fragment = new UserListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.GENDER, gender);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_list, null);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, v);
        setAdapter();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myFavoriteChange, new IntentFilter(Constant.FAVORITE_CHANGE_FILTER ));
        return v;
    }


    void openAddUserScreen(){
        Intent intent =new Intent(getActivity(), AddUserActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myFavoriteChange);
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure want to delete this user?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                (dialog, which) -> {
                    int deletedUserId = new TblUser(getActivity()).deleteUserById(userList.get(position).getUserID());
                    if (deletedUserId > 0) {
                        Toast.makeText(getActivity(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        userList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(0, userList.size());
                        checkAndVisibleView();
                    } else {
                        Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        userList.addAll(new TblUser(getActivity()).getUserListByGender(getArguments().getInt(Constant.GENDER)));
        adapter = new UserListAdapter(getActivity(), userList, new UserListAdapter.OnViewClickListener() {
            @Override
            public void onDeleteClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void onFavoriteClick(int position) {
            int lastUpdatedUser = new TblUser(getActivity()).updateFavoriteStatus(userList.get(position).getIsFavorite() == 0?1:0,userList.get(position).getUserID());
            if (lastUpdatedUser > 0){
                userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0? 1:0);
                adapter.notifyItemChanged(position);
            }
            }

            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(),AddUserActivity.class);
                intent.putExtra(Constant.USER_OBJECT, userList.get(position));
                startActivity(intent);
            }
        } );
        rcvUserList.setAdapter(adapter);
        checkAndVisibleView();
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btnAddUser)
    public void onViewClicked() {
        openAddUserScreen();

    }

    class MyFavoriteChange extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(Constant.USER_ID)){
                UserModel model = (UserModel) intent.getSerializableExtra(Constant.USER_ID);
                checkUserIDAndUpdateStatus(model.getUserID());
            }
        }
    }
    void checkUserIDAndUpdateStatus(int userID){
        for (int i = 0;i<userList.size();i++){
            if (userID == userList.get(i).getUserID()){
                int isFavorite = userList.get(i).getIsFavorite();
                userList.get(i).setIsFavorite(isFavorite == 0?1:0);
                adapter.notifyItemChanged(i);
                return;
            }
        }

    }
}
