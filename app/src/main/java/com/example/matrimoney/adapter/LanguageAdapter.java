package com.example.matrimoney.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.example.matrimoney.Model.LanguageModel;
import com.example.matrimoney.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageAdapter extends BaseAdapter {
    Context context;
    ArrayList<LanguageModel> languageList;

    public LanguageAdapter(Context context, ArrayList<LanguageModel> languageList) {
        this.context = context;
        this.languageList = languageList;
    }

    @Override
    public int getCount() {
        return languageList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ViewHolder viewHolder;
        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.view_row_text, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) v.getTag();
        }
        viewHolder.tvName.setText(languageList.get(i).getName());
        if (i == 0) {
            viewHolder.tvName.setTextColor(ContextCompat.getColor(context, R.color.colorGrey));
        } else {
            viewHolder.tvName.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));
        }
        return v;
    }

    class ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
