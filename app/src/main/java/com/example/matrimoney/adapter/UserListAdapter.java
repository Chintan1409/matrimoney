package com.example.matrimoney.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.matrimoney.Model.UserModel;
import com.example.matrimoney.R;
import com.example.matrimoney.util.Constant;
import com.example.matrimoney.util.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {
    Context context;
    ArrayList<UserModel> userList;

    OnViewClickListener onViewClickListener;
    @BindView(R.id.tvAge)
    TextView tvAge;

    public UserListAdapter(Context context, ArrayList<UserModel> userList, OnViewClickListener onViewClickListener) {
        this.context = context;
        this.userList = userList;
        this.onViewClickListener = onViewClickListener;

    }

    public interface OnViewClickListener {
        void onDeleteClick(int position);

        void onFavoriteClick(int position);

        void onItemClick(int position);
    }

    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        holder.tvFullName.setText(userList.get(position).getName() + " " + userList.get(position).getSurName());
        holder.tvLanguage.setText(userList.get(position).getLanguage() + " | " + userList.get(position).getCity());
        //holder.tvDob.setText(userList.get(position).getDob());
        String[] date = userList.get(position).getDob().split("/");
        String age = Utils.getAge(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]));


        holder.tvAge.setText("Age : " + age +"  Years");
        holder.ivFavoriteUser.setImageResource(userList.get(position).getIsFavorite() == 0 ? R.drawable.baseline_star_outline_black_36 : R.drawable.baseline_star_black_36);
        holder.ivDeleteUser.setOnClickListener(view -> {
            if (onViewClickListener != null) {
                onViewClickListener.onDeleteClick(position);
            }
        });
        holder.ivFavoriteUser.setOnClickListener(view -> {
            if (onViewClickListener != null) {
                onViewClickListener.onFavoriteClick(position);
            }

        });
        holder.itemView.setOnClickListener(view -> {
            if (onViewClickListener != null) {
                onViewClickListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullName)
        TextView tvFullName;
        @BindView(R.id.tvLanguage)
        TextView tvLanguage;
       // @BindView(R.id.tvDob)
       // TextView tvDob;
       @BindView(R.id.tvAge)
       TextView tvAge;

        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavoriteUser)
        ImageView ivFavoriteUser;




        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
