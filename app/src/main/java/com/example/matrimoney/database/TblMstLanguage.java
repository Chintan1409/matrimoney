package com.example.matrimoney.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimoney.Model.CityModel;
import com.example.matrimoney.Model.LanguageModel;

import java.util.ArrayList;

public class TblMstLanguage extends MyDatabase {
    public static final String TABLE_NAME = "TblMstLanguage";
    public static final String LANGUAGE_ID = "LanguageID";
    public static final String NAME = "Name";
    public TblMstLanguage(Context context)  {
        super(context);
    }
    public ArrayList<LanguageModel> getLanguages() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguageModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        LanguageModel languageModel1= new LanguageModel();
        languageModel1.setName("Select One");
        list.add(0, languageModel1);
        for (int i = 0; i < cursor.getCount(); i++) {
            LanguageModel languageModel= new LanguageModel();
            languageModel.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            languageModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            list.add(languageModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
    public LanguageModel getLanguageById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        LanguageModel model = new LanguageModel();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + LANGUAGE_ID + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        model.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        cursor.close();
        db.close();
        return model;
    }
}
